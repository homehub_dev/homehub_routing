mkdir val_data 
mkdir val_logs
mv central-fed-district-latest.osm.pbf val_data/
python3 ../scripts/valhalla_build_config   --mjolnir-tile-dir val_data/valhalla_tiles   --mjolnir-tile-extract ""   --mjolnir-traffic-extract ""   --mjolnir-timezone val_data/timezones.sqlite   --mjolnir-admin val_data/admins.sqlite   --loki-logging-file-name val_logs/valhalla_loki.log   --mjolnir-logging-file-name val_logs/valhalla_mjolnir.log   --meili-logging-file-name val_logs/valhalla_meili.log   --odin-logging-file-name val_logs/valhalla_odin.log   --thor-logging-file-name val_logs/valhalla_thor.log   --loki-logging-type file   --mjolnir-logging-type file   --meili-logging-type file   --odin-logging-type file   --thor-logging-type file   --mjolnir-max-cache-size 1000000000   --additional-data-elevation ""   > val_data/val_conf.json
./valhalla_build_tiles -c val_data/val_conf.json val_data/central-fed-district-latest.osm.pbf 
./valhalla_service val_data/val_conf.json 1
