#!/usr/bin/env bash

cd /build_dir/project/build/
mkdir val_data
mkdir val_logs

cp /data_dir/*.osm.pbf val_data/

osmium cat -o val_data/cat.osm.pbf val_data/*.osm.pbf
osmium sort -o val_data/cat_sorted.osm.pbf val_data/cat.osm.pbf

python3 ../scripts/valhalla_build_config \
        --mjolnir-tile-dir val_data/valhalla_tiles \
        --mjolnir-tile-extract "" \
        --mjolnir-traffic-extract "" \
        --mjolnir-timezone val_data/timezones.sqlite \
        --mjolnir-admin val_data/admins.sqlite \
        --loki-logging-file-name val_logs/valhalla_loki.log \
        --mjolnir-logging-file-name val_logs/valhalla_mjolnir.log \
        --meili-logging-file-name val_logs/valhalla_meili.log \
        --odin-logging-file-name val_logs/valhalla_odin.log \
        --thor-logging-file-name val_logs/valhalla_thor.log \
        --loki-logging-type file \
        --mjolnir-logging-type file \
        --meili-logging-type file \
        --odin-logging-type file \
        --thor-logging-type file \
        --mjolnir-max-cache-size 100000000 \
        --additional-data-elevation "" \
        --service-limits-isochrone-max-contours 6 \
  > val_data/val_conf.json

./valhalla_build_tiles -c val_data/val_conf.json val_data/cat_sorted.osm.pbf

rm val_data/*.osm.pbf