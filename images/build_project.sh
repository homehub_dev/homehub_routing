#!/bin/bash

set -e
cd project
rm -rf build
mkdir -p build
cd build

cmake -DCMAKE_BUILD_TYPE=Release -DENABLE_PYTHON_BINDINGS=OFF ..
make -j6