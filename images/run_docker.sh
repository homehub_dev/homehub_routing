#!/bin/bash

docker build -t antitail/routing:latest .
docker run \
-v /home/ail/homehub_routing/valhalla:/build_dir/project \
-v /home/ail/urbasaur/urbasaur_pipeline/run_pipeline/data:/data_dir \
-p 8002:8002 antitail/routing:latest
